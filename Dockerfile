FROM ubuntu
ADD ./script.sh .
RUN ["chmod", "7", "script.sh"]
CMD ["./script.sh"]
